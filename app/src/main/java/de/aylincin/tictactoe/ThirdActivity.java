package de.aylincin.tictactoe;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.util.HashMap;

public class ThirdActivity extends AppCompatActivity implements View.OnClickListener{

    int[] buttonIDs = new int[] {R.id.button_00, R.id.button_01, R.id.button_02, R.id.button_10, R.id.button_11, R.id.button_12, R.id.button_20, R.id.button_21, R.id.button_22};
    Button[] buttons = new Button[9];
    int[] boardValues = new int[9];
    int lastHumanMove=0;
    int totalMoves=0;
    boolean computer_moved = false;

    public static final int EMPTY_VALUE=0;
    public static final int COMPUTER_VALUE=1;
    public static final int HUMAN_VALUE=2;
    public static final String COMPUTER_CHARACTER="O";
    public static final String HUMAN_CHARACTER="X";
    public static final String EMPTY_CHARACTER="";
    public static final String NOBODY="NOBODY";

    HashMap<Integer, Boolean> losingGamePositions = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        for(int nIndex=0; nIndex<9; nIndex++) {
            buttons[nIndex] = findViewById(buttonIDs[nIndex]);
            buttons[nIndex].setOnClickListener(this);
        }
        drawBoard();
    }

    @Override
    public void onClick(View v){
        if(v instanceof Button){
            Button thisButton = (Button) v;

            int index = 0;
            for(int i = 0; i < buttonIDs.length; i++){
                if(thisButton.getId() == buttonIDs[i]) {
                    index = i;
                    break;
                }
            }

            if(boardValues[index]==EMPTY_VALUE){
                boardValues[index]=HUMAN_VALUE;
                lastHumanMove=index;
                drawBoard();
                totalMoves++;

                if(checkWinner(HUMAN_VALUE)){
                    learnFromLosing();
                    showWinner(HUMAN_CHARACTER);

                } else {
                    if(totalMoves==9)
                    {
                        showWinner(NOBODY);
                    } else {
                        doComputerTurn();
                    }

                }

            }

        }

    }

    public void showWinner(String playerID){
        if(playerID==NOBODY){
            Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
        } else if(playerID==HUMAN_CHARACTER) {
            Toast.makeText(this, "Player 1 wins!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Cheddar wins!", Toast.LENGTH_SHORT).show();
        }

        for(int nIndex=0; nIndex<9; nIndex++){
            buttons[nIndex].setText(EMPTY_CHARACTER);
            boardValues[nIndex]=EMPTY_VALUE;
            totalMoves=0;
        }

    }


    public boolean checkWinner(int playerID){
        if((boardValues[0]==playerID && boardValues[1]==playerID && boardValues[2]==playerID) ||
                (boardValues[0]==playerID && boardValues[3]==playerID && boardValues[6]==playerID) ||
                (boardValues[0]==playerID && boardValues[4]==playerID && boardValues[8]==playerID) ||
                (boardValues[1]==playerID && boardValues[4]==playerID && boardValues[7]==playerID) ||
                (boardValues[2]==playerID && boardValues[4]==playerID && boardValues[6]==playerID) ||
                (boardValues[2]==playerID && boardValues[5]==playerID && boardValues[8]==playerID) ||
                (boardValues[3]==playerID && boardValues[4]==playerID && boardValues[5]==playerID) ||
                (boardValues[6]==playerID && boardValues[7]==playerID && boardValues[8]==playerID))
            return true;
        else
            return false;
    }

    public void doComputerTurn(){

        for(int nIndex=0; nIndex<9; nIndex++){

            if(boardValues[nIndex]==EMPTY_VALUE){

                if(isOKToMove(nIndex)){
                    boardValues[nIndex]=COMPUTER_VALUE;
                    computer_moved=true;
                    totalMoves++;
                    drawBoard();
                    break;
                }

            }

        }

        if (checkWinner(COMPUTER_VALUE)) {
            showWinner(COMPUTER_CHARACTER);

        } else {
            if(!computer_moved) {

                // There are no moves, so let's flag this as a bad board position

                learnFromLosing();

                // Just do any move, and lose

                for(int nIndex=0; nIndex<9; nIndex++){

                    if(boardValues[nIndex]==EMPTY_VALUE){
                        boardValues[nIndex]=COMPUTER_VALUE;
                        computer_moved=true;
                        drawBoard();
                        break;
                    }

                }

            }

        }

    }

    public boolean isOKToMove(int thisIndex){
        int boardValue = calcBoardValue();
        boardValue+=COMPUTER_VALUE * Math.pow(3, thisIndex);

        if(losingGamePositions.containsKey(boardValue)){
            return false;
        } else {
            return true;
        }
    }

    public void learnFromLosing(){
        int losingPosition = calcBoardValue();
        losingPosition-= HUMAN_VALUE * Math.pow(3, lastHumanMove);
        losingGamePositions.put(losingPosition, true);
    }

    public int calcBoardValue(){
        int boardValue = 0;

        for(int nIndex=0; nIndex<9; nIndex++){
            boardValue += boardValues[nIndex] * Math.pow(3,nIndex);
        }
        return boardValue;
    }

    public void drawBoard(){

        for(int nIndex=0; nIndex<9; nIndex++){
            switch(boardValues[nIndex]){
                case HUMAN_VALUE:
                    buttons[nIndex].setText(HUMAN_CHARACTER);
                    break;
                case COMPUTER_VALUE:
                    buttons[nIndex].setText(COMPUTER_CHARACTER);
                    break;
                default:
                    buttons[nIndex].setText(EMPTY_CHARACTER);
            }
        }
    }
}
